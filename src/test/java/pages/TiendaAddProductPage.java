package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TiendaAddProductPage {

	private static final Logger logger = LoggerFactory.getLogger(TiendaAddProductPage.class);

	private WebElement element = null;
	private List<WebElement> listElements = null;

	public WebElement getInputProduct(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[contains(@class,'form-control')]"));
		return element;
	}

	public WebElement getButtonAdd(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(@class,'btn btn-success btn-sm')]"));
		return element;
	}

	public WebElement getButtonRemove(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(@class,'btn btn-danger btn-sm')]"));
		return element;
	}

	public WebElement getTitle(WebDriver driver) {
		element = driver.findElement(By.xpath("//h3"));
		return element;
	}

	public List<WebElement> getListProducts(WebDriver driver) {
		By byListProducts = By.xpath("//div[@class='item ng-binding ng-scope']");
		this.checkElementIsVisible(driver,byListProducts , 0);
		listElements = driver.findElements(byListProducts);
		for (WebElement webElement : listElements) {
			logger.info(webElement.getText());
		}
		return listElements;
	}

	public void checkElementIsVisible(WebDriver driver, By elment, int waitTime) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, waitTime);
			WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(elment));
		} catch (Exception e) {
			logger.info("Elment not found --> {}", e);
		}
		
	}

}
