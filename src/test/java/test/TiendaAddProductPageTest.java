package test;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.TiendaAddProductPage;

public class TiendaAddProductPageTest {

	private static final Logger logger = LoggerFactory.getLogger(TiendaAddProductPageTest.class);
	
	public WebDriver driver;
	public TiendaAddProductPage tiendaAddProductPage; 
	
	@BeforeTest
	public void setupTest() {
		String projectPath = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", projectPath+"/drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		driver.get("https://tienda-ad1b4.firebaseapp.com/");
		tiendaAddProductPage = new TiendaAddProductPage();
	}

	@Test
	public void validateTitle(){
		String title = tiendaAddProductPage.getTitle(driver).getText();
		logger.info("title --> {}",title);
		assertEquals(" Lista de productos", title);
	}
	
	@Test
	public void addProductTest () {
		tiendaAddProductPage.getInputProduct(driver).sendKeys("Product 1");
		tiendaAddProductPage.getButtonAdd(driver).click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void removeProductTest () {
		tiendaAddProductPage.getButtonRemove(driver).click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void listProdutTest() {
		List listProducts = tiendaAddProductPage.getListProducts(driver);
		logger.info("tama�o del la lista --> {}",listProducts.size());
		Assert.assertNotEquals(0, listProducts.size());
		//Assert.assertTrue(listProducts.size() > 0);
	}
	
	@AfterTest
	public void Finish() {
		driver.close();
	}

}
